# Gownitoring

gownitoring is a monitoring system using nrpe script for probes.

gownitoring is the evolution of [mownitoring](https://github.com/danieljakots/mownitoring).
This is not just a rewrite in go. mownitoring used to be a python script running
on a host (through cron(8)) using `check_nrpe` to check remotely a machine.
However, NRPE is deprecated and receive little love from their maintainers.

gownitoring is now in two parts (also it's the same binary). There is an agent
and a poller (which acts has an agent for localhost as well though). The agent
includes an HTTP server which allows the poller to run nrpe check on that
agent's host. Transport between the agent and the poller is encrypted with
mTLS. (A simple pki can be made with [gopki](https://github.com/danieljakots/gopki)).
Eventually, basic NRPE probes functionality should be included in gownitoring
itself.

## Configuration

A configuration file example is provided in this repository:
gownitoring.json.sample.

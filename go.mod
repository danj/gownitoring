module go.chown.me/gownitoring

go 1.17

require (
	go.chown.me/pushover v0.0.0-20220520012301-2a217a643c2d
	go.chown.me/smtp v0.0.0-20220520015811-cf814224491a
)

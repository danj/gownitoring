package main

import (
	"fmt"
	"time"
)

type Alert struct {
	cr      *CommandResult
	modTime time.Time
	verb    string
}

func (a Alert) Subject() string {
	var symbols string
	if a.verb == "appeared" {
		symbols = "+++"
	} else if a.verb == "disappeared" {
		symbols = "---"
	} else if a.verb == "changed" {
		symbols = "~~~"
	}
	key := key(a.cr.Hostname, a.cr.Check)
	return fmt.Sprintf("[gownitoring] %s %s", symbols, key)
}

func (a Alert) CraftBodyForEmail() string {
	preverb := "the"
	if a.verb == "appeared" {
		preverb = "a"
	}

	body := fmt.Sprintf("Hi,\nOn %s, %s problem %s for %s!%s:\n%s",
		a.modTime.Format(time.RFC3339),
		preverb, a.verb, a.cr.Hostname, a.cr.Check, a.cr.Stdout)
	return fmt.Sprintf("%s\n\nYours truly,\n-- \nGownitoring", body)
}

func (a Alert) CraftBodyForPushover() string {
	return fmt.Sprintf("%s: %s", a.modTime.Format(time.RFC3339), a.cr.Stdout)
}

func (app *Application) Notify(a Alert) {
	app.Muted.mu.Lock()
	muted := app.Muted.Muted
	app.Muted.mu.Unlock()

	if muted {
		return
	}

	err := app.Notifier.SendEmail(a.Subject(), a.CraftBodyForEmail())
	if err != nil {
		app.Logger.Printf("Couldn't send email: %v", err)
	}
	err = app.Notifier.SendPushover(a.Subject(), a.CraftBodyForPushover())
	if err != nil {
		app.Logger.Printf("Couldn't pushover: %v", err)
	}
}

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const (
	ErrorInputValidation  = "wrong input"
	ErrorInternal         = "internal error"
	ErrorMalformedJSON    = "malformed json"
	ErrorMethodNotAllowed = "method not allowed"
	ErrorNotFound         = "not found"
)

// CheckRespCode checks the http.Response has the given status code. Otherwise,
// it returns an error made with the given context string to improve the UX,
// followed by the body for debugging purpose.
func CheckRespCode(resp *http.Response, status int, context string) error {
	if resp.StatusCode != status {
		message := fmt.Sprintf("%s didn't return a %d but a %d", context, status, resp.StatusCode)

		defer resp.Body.Close()
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("%s %w", message, err)
		}
		bodyString := string(bodyBytes)
		return fmt.Errorf("%s %s", message, bodyString)
	}
	return nil
}

// HttpError is based on what http.Error() but brings json outputs.
func (app *Application) HttpError(w http.ResponseWriter, status int, error string, data map[string]interface{}) {
	response := struct {
		Status int                    `json:"status"`
		Error  string                 `json:"error"`
		Data   map[string]interface{} `json:"data,omitempty"`
	}{
		Status: status,
		Error:  error,
		Data:   data,
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	jsonData, err := json.Marshal(response)
	if err != nil {
		humanErr := fmt.Sprintf("Marshalling failed: %v", err)
		app.Logger.Print(humanErr)
		http.Error(w, fmt.Sprintf("{\"error\":\"%s\"", humanErr), http.StatusInternalServerError)
		return
	}

	app.Logger.Print(string(jsonData))

	w.WriteHeader(status)
	_, err = w.Write(jsonData)
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

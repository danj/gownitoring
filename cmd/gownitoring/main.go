package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime/debug"
	"sync"

	"go.chown.me/gownitoring/notifier"
	"go.chown.me/pushover"
	"go.chown.me/smtp"
)

type Config struct {
	CertPath       string                   `json:"CertPath"`
	KeyPath        string                   `json:"KeyPath"`
	CaPath         string                   `json:"CaPath"`
	NRPEPath       string                   `json:"NRPEPath"`
	Listen         string                   `json:"Listen"`
	Mode           string                   `json:"Mode"`
	LocalChecks    map[string]string        `json:"LocalChecks"`
	MachinesToPoll map[string]MachineToPoll `json:"MachinesToPoll"`
	SMTP           struct {
		Addr  string `json:"Addr"`
		Hello string `json:"Hello"`
		From  string `json:"From"`
		To    string `json:"To"`
	} `json:"SMTP"`
	Pushover struct {
		Token string `json:"Token"`
		User  string `json:"User"`
	} `json:"Pushover"`
}

type MachineToPoll struct {
	Port   int      `json:"Port"`
	Checks []string `json:"Checks"`
}

type Application struct {
	Config   *Config
	C        *http.Client
	chans    chans
	Muted    *MutedSafe
	Notifier Notifier
	Logger   Logger
}

type Logger interface {
	Print(...interface{})
	Printf(format string, v ...interface{})
	Fatalf(format string, v ...interface{})
}

type Notifier interface {
	SendEmail(subject, body string) error
	SendPushover(subject, body string) error
}

type MutedSafe struct {
	mu    sync.Mutex
	Muted bool
}

type chans struct {
	request chan struct{}
	answer  chan States
}

// The linker shall replace these strings
var (
	COMMIT   = "1234567"
	DATE     = "1970-01-01"
	COMPILER = "3.14"
)

func main() {
	bi, _ := debug.ReadBuildInfo()
	COMPILER = bi.GoVersion
	Config, err := readConfig(fmt.Sprintf("/etc/%s.json", ProgName()))
	if err != nil {
		log.Fatal("Can't load config: ", err)
	}

	app := &Application{Config: Config}
	l := log.New(os.Stdout, "", 0)
	app.Logger = l

	app.Logger.Printf("Starting. Commit %s built on date %s with %s\n", COMMIT, DATE, COMPILER)
	go app.Serve()

	if Config.Mode == "agent" {
		app.LogSignal()
	}

	go app.LogSignal()

	// XXX need to make it non fatal
	p, err := pushover.New(Config.Pushover.Token, Config.Pushover.User)
	if err != nil {
		app.Logger.Fatalf("Won't use Pushover: %v,", err)
	}
	s := smtp.New(Config.SMTP.Addr, Config.SMTP.Hello)
	n := notifier.New(p, s, Config.SMTP.From, Config.SMTP.To)
	app.Notifier = n

	app.CheckMachines()
}

func readConfig(configPath string) (*Config, error) {
	configFile, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}
	data := &Config{}
	if err := json.Unmarshal(configFile, &data); err != nil {
		return nil, err
	}
	return data, nil
}

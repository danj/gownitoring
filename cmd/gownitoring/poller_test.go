package main

import (
	"testing"
	"time"
)

func TestKey(t *testing.T) {
	got := key("bleh", "blah")
	shouldbe := "bleh!blah"
	if got != shouldbe {
		t.Fatalf("got %v, expected %v", got, shouldbe)
	}
}

func TestStatesString(t *testing.T) {
	var states States
	if states.String() != "States are: no known problem!" {
		t.Fatal("states.String() with empty states failed")
	}
	states = make(map[string]*State)
	state := &State{1, time.Now()}
	states["test1"] = state
	shouldbe := "States are: test1:1; "
	got := states.String()
	if got != shouldbe {
		t.Fatalf("got:\n%v\nexpected:\n%v", got, shouldbe)
	}
}

package main

import (
	"testing"
	"time"
)

func TestSubject(t *testing.T) {
	var tests = []struct {
		a    Alert
		want string
	}{
		{Alert{
			verb: "appeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_ntp",
			},
		},
			"[gownitoring] +++ example.com!check_ntp"},
		{Alert{
			verb: "disappeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_http",
			},
		},
			"[gownitoring] --- example.com!check_http"},
		{Alert{
			verb: "changed",
			cr: &CommandResult{
				Hostname: "jeancanard.com",
				Check:    "check_imap",
			},
		},
			"[gownitoring] ~~~ jeancanard.com!check_imap"},
	}

	for _, tt := range tests {
		got := tt.a.Subject()
		want := tt.want
		if got != want {
			t.Fatalf("got:\n%s\nwanted:\n%s\n", got, want)
		}
	}
}

func TestCraftBodyForEmail(t *testing.T) {
	ts, err := time.Parse(time.RFC3339, "2022-05-23T02:27:03Z")
	if err != nil {
		t.Fatal("couldn't parse date")
	}
	var tests = []struct {
		a    Alert
		want string
	}{
		{Alert{
			verb: "appeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_ntp",
				Stdout:   "broken lol",
			},
			modTime: ts,
		},
			"Hi,\nOn 2022-05-23T02:27:03Z, a problem appeared for example.com!check_ntp:\nbroken lol\n\nYours truly,\n-- \nGownitoring",
		},
		{Alert{
			verb: "disappeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_http",
				Stdout:   "OK",
			},
			modTime: ts,
		},
			"Hi,\nOn 2022-05-23T02:27:03Z, the problem disappeared for example.com!check_http:\nOK\n\nYours truly,\n-- \nGownitoring",
		},
		{Alert{
			verb: "changed",
			cr: &CommandResult{
				Hostname: "jeancanard.com",
				Check:    "check_imap",
				Stdout:   "meh",
			},
			modTime: ts,
		},
			"Hi,\nOn 2022-05-23T02:27:03Z, the problem changed for jeancanard.com!check_imap:\nmeh\n\nYours truly,\n-- \nGownitoring",
		},
	}

	for _, tt := range tests {
		got := tt.a.CraftBodyForEmail()
		want := tt.want
		if got != want {
			t.Fatalf("got:\n%s\nwanted:\n%s\n", got, want)
		}
	}
}

func TestCraftBodyForPushover(t *testing.T) {
	ts, err := time.Parse(time.RFC3339, "2022-05-23T02:27:03Z")
	if err != nil {
		t.Fatal("couldn't parse date")
	}
	var tests = []struct {
		a    Alert
		want string
	}{
		{Alert{
			verb: "appeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_ntp",
				Stdout:   "broken lol",
			},
			modTime: ts,
		},
			"2022-05-23T02:27:03Z: broken lol",
		},
		{Alert{
			verb: "disappeared",
			cr: &CommandResult{
				Hostname: "example.com",
				Check:    "check_http",
				Stdout:   "OK",
			},
			modTime: ts,
		},
			"2022-05-23T02:27:03Z: OK",
		},
		{Alert{
			verb: "changed",
			cr: &CommandResult{
				Hostname: "jeancanard.com",
				Check:    "check_imap",
				Stdout:   "meh",
			},
			modTime: ts,
		},
			"2022-05-23T02:27:03Z: meh",
		},
	}

	for _, tt := range tests {
		got := tt.a.CraftBodyForPushover()
		want := tt.want
		if got != want {
			t.Fatalf("got:\n%s\nwanted:\n%s\n", got, want)
		}
	}
}

// Not really sure how to test :(
// func TestNotify(t *testing.T) {
// 	ts, err := time.Parse(time.RFC3339, "2022-05-23T02:27:03Z")
// 	if err != nil {
// 		t.Fatal("couldn't parse date")
// 	}
// 	a := Alert{
// 		verb: "appeared",
// 		cr: &CommandResult{
// 			Hostname: "example.com",
// 			Check:    "check_ntp",
// 			Stdout:   "broken lol",
// 		},
// 		modTime: ts,
// 	}

// 	app := &Application{}

// 	app.Notify(a)
// }

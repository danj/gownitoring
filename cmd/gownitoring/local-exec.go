package main

import (
	"errors"
	"os/exec"
)

type CommandResult struct {
	Hostname string
	Check    string
	Stdout   string
	RC       int
}

func (app *Application) RunLocally(command []string) *CommandResult {
	app.Logger.Printf("Running %v", command)
	cr := &CommandResult{}
	var ee *exec.ExitError

	cmd := exec.Command(command[0], command[1:]...)
	output, err := cmd.Output()
	if err != nil {
		if errors.As(err, &ee) {
			cr.RC = ee.ExitCode()
			cr.Stdout = string(output)
			return cr
		}
		cr.RC = 3
		cr.Stdout = err.Error()
		return cr
	}
	cr.Stdout = string(output)

	return cr
}

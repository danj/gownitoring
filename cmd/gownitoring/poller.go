package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

type States map[string]*State

type State struct {
	RC      int
	ModTime time.Time
}

type CheckNRPE struct {
	Hostname string
	Port     int
	Check    string
}

// pollerInit configures the app for the polling system.
func (app *Application) pollerInit() {
	tlsConf, err := GetTLSMaterialPaths(
		app.Config.CertPath, app.Config.KeyPath, app.Config.CaPath)
	if err != nil {
		app.Logger.Fatalf("Couldn't load TLS material from disk: %v", err)
	}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConf,
		},
	}
	client.Timeout = 10 * time.Second

	app.C = client

	app.chans.request = make(chan struct{})
	app.chans.answer = make(chan States)

	app.Muted = &MutedSafe{}
}

// StateKeeper acts as a gatekeeper for the datastructure used for the states (i.e. the)
// known problems.
// See app.CheckMachines for the workflow.
func (app *Application) StateKeeper(cpcr <-chan *CommandResult, CheckNRPEs chan<- CheckNRPE,
	cncr <-chan *CommandResult, pingeds chan<- *CommandResult) {
	states := States{}
	ticker := time.NewTicker(30 * time.Second)
	for {
		select {
		// When a machine was pinged
		case cr := <-cpcr:
			app.CompareStates(states, cr)
			if cr.RC != 0 {
				pingeds <- cr
				continue
			}
			go app.CheckPingToCheckNRPE(cr, CheckNRPEs, pingeds)
		// When a nrpe probe was checked
		case cr := <-cncr:
			states = app.CompareStates(states, cr)
		case <-ticker.C:
			app.Logger.Print(states.String())
		// for /state http endpoint
		case <-app.chans.request:
			app.chans.answer <- states
		}
	}
}

// CheckPingToCheckNRPE acts as a fan-out. It queues checks for nrpe probes on the
// given machine. Between queuing, it sleeps to avoid hammering the target.
func (app *Application) CheckPingToCheckNRPE(cr *CommandResult, CheckNRPEs chan<- CheckNRPE,
	pingeds chan<- *CommandResult) {
	for _, check := range app.Config.MachinesToPoll[cr.Hostname].Checks {
		cn := CheckNRPE{
			cr.Hostname,
			app.Config.MachinesToPoll[cr.Hostname].Port,
			check,
		}
		CheckNRPEs <- cn
		time.Sleep(350 * time.Millisecond)
	}
	pingeds <- cr
}

// String returns a string representated of the states prefixed by words for humans.
func (states States) String() string {
	analysis := ""
	for k, v := range states {
		analysis += fmt.Sprintf("%s:%d; ", k, v.RC)
	}
	if analysis == "" {
		return "States are: no known problem!"
	}
	analysis = fmt.Sprintf("States are: %s", analysis)
	return analysis
}

func (app *Application) CheckPingWorker(machines <-chan string, crs chan<- *CommandResult) {
	for machine := range machines {
		crs <- app.CheckPing(machine)
	}
}

func (app *Application) CheckNRPEWorker(cns <-chan CheckNRPE, crs chan<- *CommandResult) {
	for cn := range cns {
		crs <- app.CheckNRPE(cn)
	}
}

// SleepAccordingly acts as a waiting buffer between ICMP checks. The duration of
// the wait depends on whether the machine replied to our ICMP probes. If it failed
// we wait shorter to detect faster when it comes back to life.
func SleepAccordingly(cr *CommandResult, CheckPings chan<- string) {
	night := 15 * time.Second
	if cr.RC == 0 {
		night = 90 * time.Second
		night += time.Duration(rand.Intn(60)) * time.Second
	}
	time.Sleep(night)

	CheckPings <- cr.Hostname
}

// GuessWorkerNumbers computes the number of workers for pinging machines and getting
// check nrpe results.
//
// The CheckNRPEWorker amounts must be sufficient compared to the number of
// CheckPingWorker, otherwise the program will deadlock.
func (app *Application) GuessWorkerNumbers() (int, int) {
	var maxCheck int
	machines := len(app.Config.MachinesToPoll)
	for _, v := range app.Config.MachinesToPoll {
		if len(v.Checks) > maxCheck {
			maxCheck = len(v.Checks)
		}
	}
	CheckPingWorker := machines / 2
	CheckNRPEWorker := (CheckPingWorker + 1) * maxCheck
	return CheckPingWorker, CheckNRPEWorker

}

// CheckMachines is the main loop for the monitoring. First, we create the required
// channels, spawn a number of workers and then we feed the engine with the
// machines to check.
//
// The loop begins with pinging the machine. If it doesn't ping perfectly (with loss,
// or too slowly), we don't proceed to further checks since it may not work well
// anyway. If the machine pings as expected, we proceed with the NRPE checks
// defined in the supplied configuration. For each check (ICMP or NRPE), we store any
// error.
//
// If we get a different result from the previous check(good -> bad, bad -> good,
// critical -> warning, warning -> critical), we will trigger notifications.
//
// Once we checked, we sleep for a bit and then start over.
func (app *Application) CheckMachines() {
	app.pollerInit()

	CheckPings := make(chan string)
	CheckPingCommandResults := make(chan *CommandResult)

	CheckNRPEs := make(chan CheckNRPE)
	CheckNRPECommandResults := make(chan *CommandResult)

	pingeds := make(chan *CommandResult)

	CheckPingWorkersQty, CheckNRPEWorkerQty := app.GuessWorkerNumbers()
	app.Logger.Printf("Will poll with %d check ping workers, and %d check nrpe workers",
		CheckPingWorkersQty, CheckNRPEWorkerQty)

	for i := 0; i < CheckPingWorkersQty; i++ {
		go app.CheckPingWorker(CheckPings, CheckPingCommandResults)
	}

	for i := 0; i < CheckNRPEWorkerQty; i++ {
		go app.CheckNRPEWorker(CheckNRPEs, CheckNRPECommandResults)
	}

	go func() {
		for hostname := range app.Config.MachinesToPoll {
			CheckPings <- hostname
		}
	}()
	go app.StateKeeper(CheckPingCommandResults, CheckNRPEs, CheckNRPECommandResults, pingeds)

	for pinged := range pingeds {
		go SleepAccordingly(pinged, CheckPings)
	}
}

// key provides a string concatanating the hostname and the check.
func key(hostname, check string) string {
	return fmt.Sprintf("%s!%s", hostname, check)
}

// CheckPing checks over ICMP if a machine is available using NRPE's check_ping command.
func (app *Application) CheckPing(hostname string) *CommandResult {
	timeout := "30"
	latencyWarn := "1000"
	lossWarn := "30%"
	latencyCrit := "2000"
	lossCrit := "50%"
	packets := "5"
	command := []string{app.Config.NRPEPath + "/check_ping",
		"-H", hostname,
		"-t", timeout,
		"-w", fmt.Sprintf("%s,%s", latencyWarn, lossWarn),
		"-c", fmt.Sprintf("%s,%s", latencyCrit, lossCrit),
		"-p", packets,
	}
	cr := app.RunLocally(command)
	cr.Hostname = hostname
	cr.Check = "checkPing"
	return cr
}

// CheckNRPE does an http call to a remote machine to execute and get the result of a
// NRPE check.
//
// This function doesn't return any error as any error in the process will be encoded
// in the returned *CommandResult.
func (app *Application) CheckNRPE(cn CheckNRPE) *CommandResult {
	key := key(cn.Hostname, cn.Check)
	cr := &CommandResult{}
	cr.Hostname = cn.Hostname
	cr.Check = cn.Check
	path := fmt.Sprintf("https://%s:%d/nrpe/%s", cn.Hostname, cn.Port, cn.Check)
	app.Logger.Printf("calling %s", path)
	r, err := app.C.Get(path)
	if err != nil {
		humanErr := fmt.Sprintf("couldn't GET for %s: %v", key, err)
		app.Logger.Print(humanErr)
		cr.RC = 3
		cr.Stdout = humanErr
		return cr
	}
	err = CheckRespCode(r, 200, key)
	if err != nil {
		humanErr := fmt.Sprintf("couldn't GET for %s: %v", key, err)
		app.Logger.Print(humanErr)
		cr.RC = 3
		cr.Stdout = humanErr
		return cr
	}

	err = json.NewDecoder(r.Body).Decode(cr)
	r.Body.Close()
	if err != nil {
		humanErr := fmt.Sprintf("couldn't Decode json for %s: %v", key, err)
		app.Logger.Print(humanErr)
		cr.RC = 3
		cr.Stdout = humanErr
	}

	// Do it again because trust no one
	cr.Hostname = cn.Hostname
	cr.Check = cn.Check
	return cr
}

// CompareStates checks for difference between the known state and the given
// *CommandResult. In case, it will call app.Notify() to send the relevant notifications.
func (app *Application) CompareStates(states States, cr *CommandResult) map[string]*State {
	key := key(cr.Hostname, cr.Check)

	// Was good and it's still good, best case scenario, nothing to do
	_, known := states[key]
	if !known && cr.RC == 0 {
		return states
	}

	// Not good, but no change
	if known && cr.RC == states[key].RC {
		return states
	}

	// was good, but not anymore, :oh_no:
	if !known && cr.RC != 0 {
		states[key] = &State{}
		states[key].RC = cr.RC
		states[key].ModTime = time.Now()

		a := Alert{
			cr:      cr,
			modTime: states[key].ModTime,
			verb:    "appeared",
		}

		app.Notify(a)
		return states
	}

	// wasn't good, but it's fixed now :yay:
	if known && cr.RC != states[key].RC && cr.RC == 0 {
		a := Alert{
			cr:      cr,
			modTime: time.Now(),
			verb:    "disappeared",
		}
		app.Notify(a)
		delete(states, key)
		return states
	}

	// wasn't good, it changed but it's still broken, :grimacing:
	if known && cr.RC != states[key].RC && cr.RC != 0 {
		states[key].ModTime = time.Now()
		states[key].RC = cr.RC
		a := Alert{
			cr:      cr,
			modTime: states[key].ModTime,
			verb:    "changed",
		}
		app.Notify(a)
		return states
	}

	app.Logger.Fatalf("should have been unreachable lol! I was checking: %s, was: %d, got: %d",
		key, states[key].RC, cr.RC)

	return nil
}

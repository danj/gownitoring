package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

func (app *Application) Serve() {
	tlsConf, err := GetTLSMaterialPaths(
		app.Config.CertPath, app.Config.KeyPath, app.Config.CaPath)
	if err != nil {
		app.Logger.Fatalf("Couldn't load TLS material from disk: %v", err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", app.defaultHandler)
	mux.HandleFunc("/nrpe/", app.nrpe)
	mux.HandleFunc("/states", app.states)
	mux.HandleFunc("/mute", app.mute)
	mux.HandleFunc("/version", app.version)

	srv := &http.Server{
		ReadHeaderTimeout: 2 * time.Second,
		ReadTimeout:       4 * time.Second,
		WriteTimeout:      5 * time.Second,
		IdleTimeout:       13 * time.Second,

		TLSConfig: tlsConf,

		Handler: mux,

		Addr: app.Config.Listen,
	}

	app.Logger.Printf("will listen on %s", app.Config.Listen)
	err = srv.ListenAndServeTLS("", "")
	if err != nil {
		app.Logger.Fatalf("Couldn't ListenAndServeTLS: %v", err)
	}
}

func (app *Application) logRequest(req http.Request, what string) {
	peerName := req.TLS.PeerCertificates[0].Subject.CommonName
	app.Logger.Printf("%s from %s requested %s", peerName, req.RemoteAddr, what)
}

func (app *Application) defaultHandler(w http.ResponseWriter, req *http.Request) {
	data := map[string]interface{}{"message": fmt.Sprintf("Requested path %s doesn't exist", req.URL.Path)}
	app.HttpError(w, http.StatusNotFound, ErrorNotFound, data)
}

func (app *Application) nrpe(w http.ResponseWriter, req *http.Request) {
	if len(req.URL.Path) == len("/nrpe/") {
		data := map[string]interface{}{"message": "Missing key in url (i.e. a command)"}
		app.HttpError(w, http.StatusBadRequest, ErrorInputValidation, data)
		return
	}
	key := req.URL.Path[len("/nrpe/"):]

	app.logRequest(*req, key)

	cmd, ok := app.Config.LocalChecks[key]

	if !ok {
		data := map[string]interface{}{"message": fmt.Sprintf("Requested command %v doesn't exist in the configuration", key)}
		app.HttpError(w, http.StatusBadRequest, ErrorInputValidation, data)
		return
	}

	cr := app.RunLocally(strings.Split(cmd, " "))
	data, err := json.Marshal(cr)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("Marshalling CommandResult failed: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorMalformedJSON, data)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	_, err = w.Write(data)
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

func (app *Application) states(w http.ResponseWriter, req *http.Request) {
	app.logRequest(*req, "/states")

	if app.Config.Mode == "agent" {
		data := map[string]interface{}{"message": "/states doesn't make sense in agent mode"}
		app.HttpError(w, http.StatusBadRequest, ErrorInputValidation, data)
		return
	}

	app.chans.request <- struct{}{}
	states := <-app.chans.answer

	data, err := json.Marshal(states)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("Marshalling states failed: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorMalformedJSON, data)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	_, err = w.Write(data)
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

func (app *Application) mute(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET":
		app.getMute(w, req)
	case "POST":
		app.setMute(w, req)
	default:
		w.Header().Set("Allow", "GET, POST")
		data := map[string]interface{}{"message": fmt.Sprintf("Method %v is not allowed", req.Method)}
		app.HttpError(w, http.StatusMethodNotAllowed, ErrorMethodNotAllowed, data)
	}
}

func (app *Application) getMute(w http.ResponseWriter, req *http.Request) {
	app.Muted.mu.Lock()
	muted := app.Muted.Muted
	app.Muted.mu.Unlock()

	app.logRequest(*req, "the mute status")

	data, err := json.Marshal(muted)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("Marshalling Muted failed: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorMalformedJSON, data)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	_, err = w.Write(data)
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

func (app *Application) setMute(w http.ResponseWriter, req *http.Request) {
	body, err := io.ReadAll(req.Body)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("Couldn't read body for /mute: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorInternal, data)
		return
	}

	data := &MutedSafe{}
	err = json.Unmarshal(body, data)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("couldn't unmarshal body for /mute: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorMalformedJSON, data)
		return
	}

	app.Muted.mu.Lock()
	app.Muted.Muted = data.Muted
	app.Muted.mu.Unlock()

	app.logRequest(*req, fmt.Sprintf("to set mute to %t", data.Muted))

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	_, err = w.Write([]byte("ok"))
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

func (app *Application) version(w http.ResponseWriter, req *http.Request) {
	app.logRequest(*req, "/version")

	data := make(map[string]string)
	data["compiler"] = COMPILER
	data["commit"] = COMMIT
	data["date"] = DATE

	j, err := json.Marshal(data)
	if err != nil {
		data := map[string]interface{}{"message": fmt.Sprintf("Marshalling states failed: %v", err)}
		app.HttpError(w, http.StatusInternalServerError, ErrorMalformedJSON, data)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	_, err = w.Write(j)
	if err != nil {
		app.Logger.Printf("error writing JSON body: %v", err)
	}
}

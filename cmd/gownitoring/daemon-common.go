package main

import (
	"crypto/tls"
	"crypto/x509"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func ProgName() string {
	path := os.Args[0]
	spath := strings.Split(path, "/")
	return spath[len(spath)-1]
}

// GetTLSMaterialPaths loads cert,key,ca from files and attempts to return a tls.Config
// usable for mTLS for both a client and server.
func GetTLSMaterialPaths(certPath string, keyPath string, caPath string) (
	*tls.Config, error) {
	cert, err := os.ReadFile(certPath)
	if err != nil {
		return nil, err
	}
	key, err := os.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}
	ca, err := os.ReadFile(caPath)
	if err != nil {
		return nil, err
	}

	return getTLSConfig(cert, key, ca)
}

func getTLSConfig(cert, key, ca []byte) (*tls.Config, error) {
	keyPair, err := tls.X509KeyPair(cert, key)
	if err != nil {
		return nil, err
	}

	caPool := x509.NewCertPool()
	caPool.AppendCertsFromPEM(ca)

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{keyPair},
		MinVersion:   tls.VersionTLS12,
		RootCAs:      caPool,
		ClientCAs:    caPool,
		ClientAuth:   tls.RequireAndVerifyClientCert}
	return tlsConfig, nil
}

// LogSignal catches SIGINT and SIGTERM, app.Logger. it, and shutdown. LogSignal blocks so the
// caller may need to run it inside a goroutine.
func (app *Application) LogSignal() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigs
	app.Logger.Printf("received signal: %v, shutting down", sig)
	os.Exit(0)
}

package main

import (
	"reflect"
	"testing"
)

func TestReadConfig(t *testing.T) {
	_, err := readConfig("gownitoring.json.nonexistent")
	if err == nil {
		t.Error("readConfig() nonexistent failed")
	}
	config, err := readConfig("../../gownitoring.json.sample")
	if err != nil {
		t.Error("readConfig() filetoenv failed")
	}
	shouldbe := &Config{}
	shouldbe.CertPath = "/etc/blah"
	shouldbe.KeyPath = "/etc/bleh"
	shouldbe.CaPath = "/etc/blih"
	shouldbe.NRPEPath = "/usr/local/libexec/nagios/"
	shouldbe.Listen = "127.0.0.1"
	shouldbe.Mode = "agent"

	checks := make(map[string]string)
	checks["check_load"] = "/usr/local/libexec/nagios/check_load -w 15,10,5 -c 30,25,20"
	checks["check_pxchownme"] = "/usr/local/libexec/nagios/check_http -f follow -S -p 443 -H px.chown.me"
	shouldbe.LocalChecks = checks

	MachinesToPoll := make(map[string]MachineToPoll)
	mtp := MachineToPoll{}
	mtp.Port = 6666
	mtp.Checks = []string{"check_total_procs", "check_pxchownme"}
	MachinesToPoll["example.com"] = mtp
	shouldbe.MachinesToPoll = MachinesToPoll

	shouldbe.SMTP.Addr = "smtp.example.com"
	shouldbe.SMTP.Hello = "ehlo.example.com"
	shouldbe.SMTP.From = "bleh@example.com"
	shouldbe.SMTP.To = "bleh@example.com"

	shouldbe.Pushover.Token = "abc"
	shouldbe.Pushover.User = "def"

	if !reflect.DeepEqual(config, shouldbe) {
		t.Fatalf("config parsing failed: got\n%v\nexpected\n%v", config, shouldbe)
	}

}

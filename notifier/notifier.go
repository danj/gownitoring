package notifier

import (
	"go.chown.me/pushover"
	"go.chown.me/smtp"
)

type Notifier struct {
	p    *pushover.Pushover
	s    *smtp.SMTP
	from string
	to   string
}

func New(p *pushover.Pushover, s *smtp.SMTP, from, to string) Notifier {
	return Notifier{p, s, from, to}
}

func (n Notifier) SendEmail(subject, body string) error {
	email, err := smtp.NewEmail(n.from, n.to, subject, body)
	if err != nil {
		return err
	}
	return n.s.SendEmail(email)
}

func (n Notifier) SendPushover(subject, body string) error {
	return n.p.Notify(subject, body)
}
